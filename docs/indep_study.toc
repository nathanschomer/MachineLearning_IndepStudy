\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Background and Purpose}{3}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Caffe Basics}{3}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Net Computations}{3}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}The Loss Function}{3}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}The solver}{4}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}GOUTRN}{5}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Method Overview}{5}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}The Network}{5}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Tracking}{7}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Performance Analysis}{7}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Changes}{8}{subsection.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Motion Model Update}{8}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}HDF5 Input and Classification}{8}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Generating HDF5 Data}{8}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Classifying HDF5 Data.}{9}{subsection.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}Deploying the Model}{11}{subsection.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Images \& IMU Data}{13}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Future Work}{15}{section.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Useful Links}{15}{section.8}
