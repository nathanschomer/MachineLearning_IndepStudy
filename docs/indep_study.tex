\documentclass{article}
\usepackage{listings}
\usepackage{float}
\usepackage{hyperref}
\usepackage{pdfpages}

\usepackage{graphicx}
\usepackage{subfig}

\usepackage[backend=biber]{biblatex} 
\addbibresource{citation.bib}

\graphicspath{{images/}}

\begin{document}

\title{Independent Study, Autumn 2017}
\author{Nathan Schomer}
\maketitle
\pagebreak

\tableofcontents
\listoffigures

\pagebreak
\section{Background and Purpose}

The purpose of this independent study was to expand my knowledge and
understanding of machine learning algorithms which utilize neural networks.
This knowledge will be critical as I begin my senior design project at Drexel
University. The project involves optimizing a generic object tracking 
algorithm for mobile platforms. At the heart of this algorithm is a neural
network used to classify object displacement between frames. Prior to this 
independent study, my understanding of the training and use of neural 
networks was limited to just a brief introduction. 

This document essentially contains my notes from the study- information
I hope will be useful in the following terms as I work on the senior design project.
Included with this document should be a source folder of code which is referenced
throughout this document. A repository was created to contain both documentation
and code which can be accessed at the link below. \\


\url{https://gitlab.com/nathanschomer/MachineLearning_IndepStudy.git}


\section{Caffe Basics}

\begin{itemize}
	\item{Blob- standard data wrapper in caffe. Essentially, and
		N-dimensional array which provides synchronization
		between CPU and GPU memory spaces.}
	\item{Layer- "fundamental unit of computation"; layers perform
		operations like convolution, pooling, inner products, and ReLU.
		Each layer definition includes setup, forward, and backwards.}
	\item{Net- a set of layers defined in a protobuf file.}
\end{itemize}

%\subsection{Layer Types}

%Data Layers
%\begin{itemize}
%	\item{Image Data- input raw images}
%	\item{Database- input data from LEVELDB or LMDB}
%	\item{HDF5 Input- read HDF5 data of arbitrary dimensions}
%	\item{HDF5 Output- write out HDF5 data}
%	\item{Window Data- read window data file}
%	\item{Memory Data- read data from memory}
%	\item{Dummy Data- for static data and debugging}
%\end{itemize}

\subsection{Net Computations}

A net can have 2 types of passes: forward (inference) and backward (learning). During the forward pass, the network computes the nets output for the given input which moves from bottom to top. During the backward pass, Caffe computes the gradient of the whole model which moves from top to bottom.

\subsection{The Loss Function}

Learning is based on the optimization of a loss/ objective function. This function is a mapping between weights in the model and a value describing the effectiveness of these weights.

\subsection{The solver}

Caffe's solver optimized model weights by minimizing cost function across multiple epochs (network passes). Types of Caffe solvers include:\\

\begin{itemize}
	\item{Stochastic Gradient Descent (SGD)}
	\item{AdaDelta}
	\item{Adaptive Gradient (AdaGrad)}
	\item{Adam}
	\item{Nesterov's Accelerated Gradient (Nesterov)}
	\item{RMSProp}
\end{itemize}


\noindent On each iteration, the solver:

\begin{enumerate}
	\item{performs forward pass of network to compute output + loss}
	\item{performs backward pass of network to compute gradients}
	\item{updates model weights based on gradients}
	\item{updates solver state}
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\pagebreak
\section{GOUTRN}

GOTURN\cite{goturn} is a generic object tracker which is trained entirely offline. At the heart
of this tracker is a neural network. The following section explores the model
architecture along with current methods of training and testing.

\subsection{Method Overview}
\begin{enumerate}
	\item{Input image of target object.}
	\item{Crop and scale first frame to be centered on target. Note-
		this crop is padded in order to include some background data.}
	\item{Choose a search region in current frame based on objects prev location.}
	% 	- search region can vary based on motion model used
	\item{Pass this search region through the network to find target in current frame.}
	\item{Network will output predicted pixel location of target object.}

\end{enumerate}

\subsection{The Network}

Inputs to the network include two crops- a crop to the target location in the previous frame and a crop to the search region of the current frame. During training the network learns to find the target in the current frame based on the difference between these two crops (current and previous). The network uses these two inputs to
generated a predicted target location in the current frame, given as the corners of a bounding box. The network used by GOTURN can be seen in Figure \ref{fig:goturn_net}.
This network is based on the CaffeNet network, which is shown in Figure \ref{fig:caffe_net}.

Both networks follow a similar structure- a stack of convolutional layers which provide input to fully-connected layers. The convolutional layers extract high-level features from the input image. These features are then learned by the fully-connected layers. The major difference between GOTURN's neural network and CaffeNet is the addition of a second stack of convolutional layers in parallel with the original stack. This second set of layers allows the input of two frames (previous and current). 

% - input target object + search region
% - convolutional layers determine high-level features which represent image
% - network learns difference between these two frames

% - convolutional layers taken from first 5 layers of CaffeNet architecture
% - concatenate output of conv layers (pool5 features) into a vector
% - input feature vector into 3 fully connected layers (ea w 4096 nodes)
% - connect last full connected layer to an output layer w 4 nodes which
%	represent output bounding box of detected target object
% - Scale output by factor of 10 (chosen using validation set)
% - hyperparameters taken from CaffeNet defaults
% - between each fully-connected layer we dropout and ReLU non-linearities
%	(same as CaffeNet)

\pagebreak
\begin{figure}[H]
  \centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[height=18cm]{CaffeNet}
    \caption{CaffeNet Network}
    \label{fig:caffe_net}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[height=18cm]{goturn_net2}
    \caption{GOTURN Network}
    \label{fig:goturn_net}
  \end{minipage}
\end{figure}

\subsection{Tracking}

Tracker is initialized with a bounding box of initial target object location.
For each frame that follows, input crops from the previous frame (frame n-1)
and current frame (frame n). The tracker outputs a bounding box location of 
predicted target location in current frame.

\subsection{Performance Analysis}

As stated previously, the objective of this project is to create
a mobile, generic object tracker. To make GOTURN mobile, it will be running on
a NVIDIA Tegra TX2. The TX2 is a credit card-sized single board computer which
has an embedded GPU. Since neural networks inherently perform the best on GPU's,
the TX2 provides a low-cost method of using GOTURN on a mobile platform. 

In Figure \ref{fig:perf}, a comparison of GOTURN framerates on different platforms
is shown. The two left bars show the similar frame rates of GOTURN running on the
TX2 computer and the GTX1050 in my laptop. The right bar is the framerate claimed by
the original authors of GOTURN who used a Titan X GPU. The original data was collected
using the same datasets and same code on each computer. The code used used the pre-trained,
unmodified version of GOTURN.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{goturn_performance}
\caption{Comparison of GOTURN performance on different GPU's.}
\label{fig:perf}
\end{figure}

\subsection{Changes}

In order to optimize GOTURN for the NVIDIA Tegra TX2, some changes will have to be made. 
The first change will happen to the motion model. GOTURN uses a motion model in both
training and during run-time. GOTURN trains on images by randomly transforming the image
between successive frames to simulate object movement. GOTURN also uses a motion model 
when selecting the current-frame crop to pass through the network. Updates to these motion
models will allow us to leverage domain-specific knowledge when specializing the tracker
for a specific mobile platform.

The second change to the network will include the addition of IMU data. This data will be 
added as an additional input to GOTURN's neural network. The network will be able to learn
the relationship between target object movement and observer motion. 

\section{Motion Model Update}

As discussed previously, both the runtime and the training motion models will be updated.
Updating the training motion model will specialize the Caffe model by only training
on the type of motion that will be seen from the observer and target. For example, 
if the observer and target are both ground vehicles, we can assume limited vertical movement
of the target within frame and give greater weight to horizontal movements. 

% TODO: make changes to training motion model?

\section{HDF5 Input and Classification}
One of the changes to be made is the addition of IMU data to the GOTURN
neural network. However, Caffe is tailored toward deep learning applications 
and thus, its data containers primarily support images. When including IMU
data in GOTURN's nerual network, it will have to be inputted as HDF5. Which
I have never worked with before in Caffe. I followed the tutorial 
"HDF5-Classification-Caffe"
(\url{http://melvincabatuan.github.io/HDF5-Classification-Caffe/}).

\subsection{Generating HDF5 Data}

To begin, HDF5 data files had to be generated. This was done with the 
SciKit Learn libraries in python. This was done in "data\_gen()" within
hdf5\_classification.py. The data was then split into training and testing
and plotted using matplot lib as shown in Figure \ref{fig:data}. Fortunately,
Python has a built-in support for HDF5. I used these libraries to write
the data to HDF5 files.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{hdf5_data}
\caption{Plot of generated HDF5 data}
\label{fig:data}
\end{figure}

\subsection{Classifying HDF5 Data.}

The model was trained using the Caffe module in Python. The associated code
is within lear\_and\_test() in the hdf5\_classification.py file. The model
was first trained using the network shown in \ref{fig:net} and the following
solver parameters:

\begin{itemize}
	\item test\_interval: 1000
	\item test\_iter: 250
	\item display: 1000
	\item base\_lr: 0.01
	\item max\_iter: 10000
	\item lr\_policy: "step"
	\item gamma: 0.1
	\item momentum: 0.9
	\item weight\_decay: 0.0005
	\item stepsize: 5000
	\item snapshot: 10000
	\item solver\_mode: GPU
\end{itemize}

\vspace{1.5cm}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{hdf5_net}
\caption{Network used to train model.}
\label{fig:net}
\end{figure}

\pagebreak
After training, the test dataset was passed through the network.
The resulting accuracy is shown in Figure \ref{fig:acc}.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{training}
\caption{Accuracy after training and testing.}
\label{fig:acc}
\end{figure}

\subsection{Deploying the Model}

Training and testing the model is necessary but not inherently useful.
To actually use the model, it has to be deployed. The steps of deploying a
model are as follows:

\begin{enumerate}
	\item{Edit data layer. Labels will no longer be inputted.}
	\item{Set up the network to accept data w/o labels.}
	\item{Remove any layer related to labels.}
	\item{Have network output the result.}
\end{enumerate}

% TODO: write about deploying model
Following these steps, the first change made was to the prototxt file.
The top two data layers were removed. These layers provided the network
with data for both training and testing. However, they linked whole
data sets to the network for the training and testing phases.
Because of this, this original data layer doesn't work when just one data
point is passed through the network. To fix this, a new data layer was added
to the top of the network. This layer defined the data as an input with 
shape of 1x1x1x4. These dimensions are determined by the data the model 
was originally trained on. In this case, it was trained on vectors of
length 4. 

Next, layers related to data labels had to be removed. In this network,
the only layer in this category was the "SoftmaxWithLoss" layer. The only
changes made to this layer was changing the layer type to "Softmax" and 
removing the bottom link that linked to the labels layer.

The resultant network is shown in Figure \ref{fig:deployNet}. The only
difference from the original network shown in Figure \ref{fig:net} is the
missing link from the data layer to the loss layer. This link is what
linked the labels to the loss layer.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{hdf5_deploy}
	\caption{Network Used for classification after deployment.}
	\label{fig:deployNet}
\end{figure}

\vspace{0.5cm}

After these changes to the network, a new interface was written to 
propagate data through the network and retrieve the result. The 
classifier is defined by calling caffe.Net and providing the network
prototxt file along with trained caffemodel model. Data is then provided
to the netowrk and net.forward() is called to pass the data through
the network. The results are given as probabilities for each class label.
The label with the highest probability is assigned to the input data.
This code can be seen in the hdf5\_deploy.py source file. An example
output from passing [0.3, 0.2, 0.1, 0..7] to the network is shown
in Figure \ref{fig:deployNet}.

\vspace{1cm}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{deploy}
	\caption{Classifying random data using trained model.}
	\label{fig:deploy}
\end{figure}

\section{Images \& IMU Data}
% TODO: discuss issues with data collection/ labeling 

In order to input IMU data into the GOUTRN network, a few changes
have to be made. The first is the definition of the IMU data as an
input to the network. This is specified at the top of the prototxt
file with dimensions of 1x1x1x9 since the the IMU has 9 channels 
of data. 

The CNN layers extract features which are then learned by the fully-
connected layers. Because the IMU data is essentially 9 decimal values, 
there is no need to pass them through a convolutional network like the images.
The IMU data can be passed directly into the bottom of the first fully-
connected layer. The stack of fully-connected layers will learn the 
relationship between object movement (position difference between current
and previous frame) and the inputted IMU data. 

This adddition can be seen in the difference between Figure \ref{fig:goturnNet2}
and Figure \ref{fig:netIMU}. The changes described above are shown as the 
additional link between the inputs at the top of the network and the
top fully-connected layer.

Now that the network is updated, the next step is to retrain the 
Caffe model with images and associated IMU data. The original
GOTURN model was trained and tested with the ALOV and Imagenet 
datasets. These are collections of images and videos with associated
groundtruth data. However, these datasets do not include any type of
movement or pose information about the observer so different data sets
will have to be used for the retraining of the updated network. Unfortunately,
no public data sets of images/ videos + IMU data are currently
available (that I could find). Because of this, new datasets will
have to be collected and labeled.

An MPU6040 IMU was purchased along with a NVIDIA Tegra TX2 and a
Chameleon 3 PointGrey camera. The camera was attached to the TX2
via USB3.0 cable. The IMU was interfaced with the TX2 over an I2C
bus. This was not as simple as plugging in the IMU. Various changes
were made to the device tree and the kernel had to be recompiled with
additional I2C drivers. 

A data collection program was written in C++. This software collected image
frames from the camera using OpenCV and then wrote these images to disk.
after each image grab, the software would request data from the IMU registers.
This data was written to a vector which stored the IMU data along with the 
frame number. At the completion of data collection, the vector holding the
IMU data and associated frame numbers is written to an HDF5.

\pagebreak
\begin{figure}[H]
  \centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[height=18cm]{goturn_net2}
    \caption{GOTURN Original Network}
    \label{fig:goturnNet2}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[height=18cm]{netIMU}
    \caption{GOTURN Network with IMU}
    \label{fig:netIMU}
  \end{minipage}
\end{figure}


%\section{Relevant Research}
% TODO: discuss paper from Bethany

\section{Future Work}
% TODO: discuss future work

Unfortunately, the 10 week duration of this independent study was not
sufficient time to collect and label all the image + sensor data necessary to
train a new model using the modified GOTURN network. Data labeling will
be the most tedious step of this process since each image needs to be labeled
with a bounding box around the target object. Once retraining is completed,
there are still additional steps before IMU data is completely integrated
into the tracker. 

After data collection and labeling, the first step will be to train and test a new
model using the updated network and the new dataset. Once a new model is available,
the front-end of the GOTURN tracker will have to be rewritten to include retrieving
and passing IMU data to the network.

Beyond just integrating the IMU data, other changes will have to be made. The first
and potentially most beneficial will be updating the network's hyperparameters. As
stated in the GOTURN paper, the original authors just used the default settings from
CaffeNet. They performed no optimizations to the learning rate, rate decay, or other
related parameters. There is potential to improve upon accuracy with changes to these
settings. 

\section{Useful Links}
\begin{itemize}
	\item \href{https://github.com/BVLC/caffe/wiki/Using-a-Trained-Network:-Deploy}
		   {Using a Trained Caffe Network}
	\item \href{http://melvincabatuan.github.io/HDF5-Classification-Caffe/}
		   {Classifying HDF5 in Caffe}
	\item \href{http://caffe.berkeleyvision.org/}
		   {Caffe Main Page}
	\item \href{https://support.hdfgroup.org/HDF5/}
		   {HDF5 Docs}
\end{itemize}


\pagebreak
\printbibliography

\end{document}

