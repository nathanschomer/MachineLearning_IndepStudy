import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
 
objects = ('NVIDIA Tegra TX2', 'GeForce GTX 1050', 'GeForce GTX 1080', 'GeForce GTX Titan X')
y_pos = np.arange(len(objects))
performance = [25.456, 25.9748, 46.329, 100.0]
 
plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects)
plt.ylabel('Usage')
plt.title('GOTURN Performance')
 
plt.show()
