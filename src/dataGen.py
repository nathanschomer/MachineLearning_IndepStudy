
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import caffe
import os
import h5py
import shutil
import tempfile

import sklearn
import sklearn.datasets
import sklearn.linear_model
from sklearn.model_selection import train_test_split


if __name__=='__main__':

    # generate some random data
    X, y = sklearn.datasets.make_classification(
	n_samples=10000, n_features=4, n_redundant=0, n_informative=2, 
	n_clusters_per_class=2, hypercube=False, random_state=0
    )

    # Split into train and test
    X, Xt, y, yt = train_test_split(X, y)

    # Visualize sample of the data
    ind = np.random.permutation(X.shape[0])[:1000]
    df = pd.DataFrame(X[ind])
    pd.plotting.scatter_matrix(df, figsize=(9, 9), diagonal='kde', marker='o', s=40, alpha=.4, c=y[ind])
    plt.show()

    # Train and test the scikit-learn SGD logistic regression.
    clf = sklearn.linear_model.SGDClassifier(
    	loss='log', n_iter=1000, penalty='l2', alpha=1e-3, class_weight='balanced')

    clf.fit(X, y)
    yt_pred = clf.predict(Xt)
    print('Accuracy: {:.3f}'.format(sklearn.metrics.accuracy_score(yt, yt_pred)))

    # Write out the data to HDF5 files
    dirname = os.path.abspath('./data')
    if not os.path.exists(dirname):
	os.makedirs(dirname)

    train_filename = os.path.join(dirname, 'train.h5')
    test_filename = os.path.join(dirname, 'test.h5')

    # HDF5DataLayer source should be a file containing a list of HDF5 filenames
    with h5py.File(train_filename, 'w') as f:
	f['data'] = X
	f['label'] = y.astype(np.float32)
    with open(os.path.join(dirname, 'train.txt'), 'w') as f:
	f.write(train_filename + '\n')
	f.write(train_filename + '\n')
	
    # compress the files
    comp_kwargs = {'compression': 'gzip', 'compression_opts': 1}
    with h5py.File(test_filename, 'w') as f:
	f.create_dataset('data', data=Xt, **comp_kwargs)
	f.create_dataset('label', data=yt.astype(np.float32), **comp_kwargs)
    

    with open(os.path.join(dirname, 'test.txt'), 'w') as f:
	f.write(test_filename + '\n')  
