import caffe
import shutil
import tempfile

import sklearn
import sklearn.datasets
import sklearn.linear_model
from sklearn.model_selection import train_test_split


def learn_and_test(solver_file):
    caffe.set_mode_cpu()
    solver = caffe.get_solver(solver_file)
    solver.solve()

    accuracy = 0
    test_iters = int(len(Xt) / solver.test_nets[0].blobs['data'].num)
    for i in range(test_iters):
        solver.test_nets[0].forward()
        accuracy += solver.test_nets[0].blobs['accuracy'].data
    accuracy /= test_iters
    return accuracy

if __name__=='__main__':
	network_path = 'hdf5_classification/solver.prototext';
	acc = learn_and_test(network_path)
	print("Accuracy: {:.3f}".format(acc))
