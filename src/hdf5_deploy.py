import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import caffe
import os
import h5py
import shutil
import tempfile

import sklearn
import sklearn.datasets
import sklearn.linear_model
from sklearn.model_selection import train_test_split
import sys

if __name__=='__main__':
	network = './hdf5_classification/deploy.prototxt'
	model   = './hdf5_classification/data/train_iter_10000.caffemodel'
	net = caffe.Net(network, model, caffe.TEST)

	net.blobs['data'].data[...] = [0.3, 0.2, 0.1, 0.7]
	out = net.forward()
	print('\n>> Predicted Label: {}'.format(out['loss'][0].argmax()))
